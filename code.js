let btn   = document.getElementById("btn")
let btn1  = document.getElementById("btn1")
let list  = document.querySelector("#list");
const url = 'https://treinamentoajax.herokuapp.com/messages'
//POST
btn1.addEventListener("click", (e) => {
    e.preventDefault()
    let name = document.getElementById("name").value
    let msg  = document.getElementById("msg").value

    let fetchbody = {
        message: {
            name: name,
            message: msg  
        }
    }
    
    let fetchconfig = {
        method: 'POST',
        headers:{
            "Content-Type": "application/JSON"
        }, 
        body: JSON.stringify(fetchbody)
    }
    fetch(url, fetchconfig)
    .then(response => response.json())
    .then(response => {
        let item = document.createElement("li");
        item.classList.add("item");
        item.innerHTML =`
            <div class="box">
                <h1>${response.name}</h1>
                <p>${response.message}</p>
            </div>`;
        list.appendChild(item)
    })

})

//GET
btn.addEventListener("click", (e) => {
    e.preventDefault()
    fetch(url)
    .then((response) =>{
        return response.json()
    })
    .then((response) =>{
        response.forEach((user) =>{
            let item = document.createElement("li");
            item.classList.add("item");
            item.innerHTML =`
                <div class="box">
                    <h1>${user.name}</h1>
                    <p>${user.message}</p>
                </div>`;

            list.appendChild(item);

        })
    })
})

//DELETE
fetch(url, {method: "DELETE"})
.then(console.log)
item.classList.remove("item")


//PUT
let fetchBody   = {
   "message": {
        "message": "ok"
   }
}
let fetchConfig = {
    
    method: "PUT",
    headers: {"Content-Type": "application/JSON"},
    body: JSON.stringify(fetchBody)

}
.then((response) =>{
    response.forEach((user) =>{
        let item = document.createElement("li");
        item.classList.add("item");
        item.innerHTML =`
            <div class="box">
                <h1>${user.name}</h1>
                <p>${user.message}</p>
            </div>`;
 
        list.appendChild(item);

    })
})

fetch(url, fetchConfig)
.then(console.log)